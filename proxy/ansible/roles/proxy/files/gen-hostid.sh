#!/bin/sh
#
# Generate the unique host identifier for this system.
# This happens once at post-install time.
#

hostid_file=/etc/live/hostid
[ -e $hostid_file ] && exit 0

hostid=$(od -vN 8 -An -tx1 /dev/urandom | tr -d ' ')
echo $hostid > $hostid_file

exit 0
