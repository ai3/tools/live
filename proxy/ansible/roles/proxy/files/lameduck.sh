#!/bin/sh
#
# Switch the service into 'lame duck' mode:
# * agent will report host as not available
# * nginx will redirect all requests back to the main domain
#

# Remove readiness guard file.
rm -f /run/live-ready

# Switch the NGINX configuration to the lameduck one.
echo "switching NGINX config to lameduck"
rm -f /etc/nginx/nginx.conf
ln -s nginx.conf.lameduck /etc/nginx/nginx.conf

# Reload NGINX.
echo "reloading NGINX"
kill -HUP $(cat /run/nginx.pid)

# Wait 5 minutes.
echo "sleeping for 5 minutes"
sleep 300

exit 0
