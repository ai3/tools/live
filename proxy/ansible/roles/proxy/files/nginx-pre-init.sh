#!/bin/sh
#
# Pre-initialization step for NGINX, which resets lameduck mode in
# case it was left over from a previous installation.
#

rm -f /etc/nginx/nginx.conf

safe_conf=default
if [ -e /etc/nginx/fullchain.pem ]; then
    safe_conf=ssl
fi
ln -s nginx.conf.${safe_conf} /etc/nginx/nginx.conf

exit 0
