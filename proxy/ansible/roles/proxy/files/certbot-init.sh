#!/bin/sh
#
# Run certbot once to obtain the initial SSL certificate.
# Expect that the currently active NGINX configuration will allow
# public access to /var/www/html.
#
# Create links from the 

hostid=$(cat /etc/live/hostid)
if [ -z "$hostid" ]; then
    echo "hostid file is empty!" >&2
    exit 1
fi

. /etc/live/env

domain="${hostid}.${BASE_DOMAIN}"

# Run certbot the first time if there is no SSL certificate for our
# public hostname. We run this in a loop because we have to wait until
# the hostname DNS entry is created, which is triggered by the agent
# which at this point should be running in the background.
cert_file="/etc/letsencrypt/live/${domain}/fullchain.pem"
if [ ! -e ${cert_file} ]; then
    echo "attempting to obtain SSL certificate"
    while true; do
        certbot certonly --agree-tos -m "${ACME_EMAIL}" \
                --no-directory-hooks --non-interactive \
                --domain "$domain" \
                --webroot --webroot-path /var/www/html
        if [ $? -gt 0 ]; then
            echo "certbot failed, waiting a while..." >&2
            sleep 3
        fi
    done
fi

# Switch the NGINX configuration to the SSL-enabled one.
echo "switching NGINX config to SSL-enabled"
rm -f /etc/nginx/nginx.conf \
   /etc/nginx/fullchain.pem \
   /etc/nginx/privkey.pem \
   2>/dev/null
ln -s nginx.conf.ssl /etc/nginx/nginx.conf
ln -s /etc/letsencrypt/live/${domain}/fullchain.pem /etc/nginx/fullchain.pem
ln -s /etc/letsencrypt/live/${domain}/privkey.pem /etc/nginx/privkey.pem

# Reload NGINX.
echo "reloading NGINX"
kill -HUP $(cat /run/nginx.pid)

# Tell the agent that we're ready to serve.
echo 1 > /run/live-ready

exit 0
