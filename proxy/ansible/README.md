Ansible role to set up a generic VM image with a reverse proxy that
participates in the live.a.o pool. The image should be able to do the
following automatically and without any intervention, on first boot:

* generate a unique persistent host identifier for the VM
* start the agent and register with the central registry, so that
  a DNS entry for the VM is created
* acquire a valid SSL certificate using Letsencrypt
* advertise itself as ready-to-serve and start receiving traffic

While on shutdown it should implement the following process:

* advertise status as not-ready-to-serve
* switch NGINX configuration to serve redirects back to the main
  server, to avoid breaking streams for clients
* after some time (currently 5 minutes), actually shut down the VM

The above is implemented via systemd dependencies and simple shell
scripts.
