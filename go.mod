module git.autistici.org/ai3/tools/live

require (
	git.autistici.org/ale/lb v0.0.0-20200515105224-e779f2bd45f4
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/golang/protobuf v1.4.1
	github.com/miekg/dns v1.1.29
	github.com/papertrail/go-tail v0.0.0-20180509224916-973c153b0431
	github.com/spf13/pflag v1.0.5 // indirect
	google.golang.org/grpc v1.29.1
)
