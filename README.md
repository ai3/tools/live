ai3/tools/live
===

A toolkit to manage a RTMP/HLS-based video streaming service, with few
streams and many listeners. The toolkit will autoscale a pool of
reverse HTTP proxies, which will relay content from a single streaming
server which takes care of the RTMP-to-HLS conversion.

## Auto-scaling

Auto-scaling works by managing a pool of cloud instances via an
API. Instances are scaled up and down based on the global utilization
of the entire pool (compared to thresholds, with hystheresis).

Instances report their utilization to a centralized service. This
service:

* serves DNS requests for specific instances
* serves HTTP requests by redirecting clients to a specific instance
  based on the output of load-balancing algorithm


