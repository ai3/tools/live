package autoscaler

import (
	"context"
	"log"
	"time"
)

var (
	rpcTimeout   = 60 * time.Second
	tickInterval = 5 * time.Second
)

type HostInfo struct {
	Name string
	IPv4 string
}

type backend interface {
	ListHosts(context.Context) ([]*HostInfo, error)
	CreateHost(context.Context) (*HostInfo, error)
	DeleteHost(context.Context, string) error
}

const alpha = 0.8

type Autoscaler struct {
	backend backend
	ch      chan struct{}

	curFullness float64

	// List of currently known hosts. We maintain a list of
	// "tombstones" for deleted hosts to account for the fact that
	// they might not disappear right away once we send the
	// shutdown command.
	hosts           []*HostInfo
	tombstones      map[string]struct{}
	numHosts        int
	coolOffDeadline time.Time

	// Max / min number of hosts to run.
	MaxHosts, MinHosts int

	// The target global fullness before we trigger autoscaling up.
	TargetFullnessUp float64

	// The target global fullness before we trigger autoscaling down.
	TargetFullnessDown float64

	// Cool-off period after every operation.
	CoolOffTimeout time.Duration
}

func New(backend backend) *Autoscaler {
	return &Autoscaler{
		backend:            backend,
		ch:                 make(chan struct{}),
		tombstones:         make(map[string]struct{}),
		MinHosts:           0,
		MaxHosts:           10,
		TargetFullnessUp:   0.7,
		TargetFullnessDown: 0.66,
		CoolOffTimeout:     5 * time.Minute,
	}
}

func (a *Autoscaler) Start() {
	ctx, cancel := context.WithTimeout(context.Background(), rpcTimeout)
	a.updateHosts(ctx)
	cancel()

	go func() {
		tick := time.NewTicker(tickInterval)
		defer tick.Stop()
		for {
			select {
			case now := <-tick.C:
				ctx, cancel = context.WithTimeout(context.Background(), rpcTimeout)
				a.tick(ctx, now)
				cancel()
			case <-a.ch:
				return
			}
		}
	}()
}

func (a *Autoscaler) Stop() {
	close(a.ch)
}

func (a *Autoscaler) updateHosts(ctx context.Context) {
	hosts, err := a.backend.ListHosts(ctx)
	if err != nil {
		log.Printf("error updating host list: %v", err)
		return
	}
	a.hosts = hosts
	n := 0
	for _, h := range hosts {
		if _, ok := a.tombstones[h.Name]; !ok {
			n++
		}
	}
	a.numHosts = n
}

func (a *Autoscaler) getFullness() float64 {
	return 0
}

func (a *Autoscaler) pickOneHost() *HostInfo {
	for _, h := range a.hosts {
		if _, ok := a.tombstones[h.Name]; !ok {
			return h
		}
	}
	return nil
}

// Examine the state of the system, make a decision.
func (a *Autoscaler) tick(ctx context.Context, now time.Time) {
	fullness := alpha*a.getFullness() + (1.0-alpha)*a.curFullness
	a.curFullness = fullness

	switch {
	case fullness > a.TargetFullnessUp:
		if a.numHosts >= a.MaxHosts {
			log.Printf("would like to scale up but we've reached max_hosts")
			return
		}
		if now.Before(a.coolOffDeadline) {
			log.Printf("would like to scale up but we are in the cool-off period")
			return
		}
		log.Printf("fullness at %v, scaling up...", fullness)
		host, err := a.backend.CreateHost(ctx)
		if err != nil {
			log.Printf("error trying to create new host: %v", err)
			return
		}
		log.Printf("created new host %s", host.Name)

	case fullness < a.TargetFullnessDown:
		if a.numHosts <= a.MinHosts {
			log.Printf("would like to scale down but we've reached min_hosts")
			return
		}
		if now.Before(a.coolOffDeadline) {
			log.Printf("would like to scale down but we are in the cool-off period")
			return
		}
		log.Printf("fullness at %g, scaling down...", fullness)
		host := a.pickOneHost()
		if host == nil {
			log.Printf("error deleting a host: could not find a host to delete")
			return
		}
		if err := a.backend.DeleteHost(ctx, host.Name); err != nil {
			log.Printf("error deleting a host: %v", err)
			return
		}
		a.tombstones[host.Name] = struct{}{}

	default:
		return
	}

	// We've made a change to the hosts, update our list.
	a.updateHosts(ctx)
	a.coolOffDeadline = now.Add(a.CoolOffTimeout)
}
