//go:generate protoc --go_out=plugins=grpc,paths=source_relative:. -I. -I../../../../.. live.proto
package livepb

import "net"

func (n *NodeInfo) GetIPv4Addrs() []net.IP {
	out := make([]net.IP, 0, len(n.IpAddrs))
	for _, addr := range n.IpAddrs {
		ip := net.ParseIP(addr)
		if ip != nil && ip.To4() != nil {
			out = append(out, ip)
		}
	}
	return out
}

func (n *NodeInfo) GetIPv6Addrs() []net.IP {
	out := make([]net.IP, 0, len(n.IpAddrs))
	for _, addr := range n.IpAddrs {
		ip := net.ParseIP(addr)
		if ip != nil && ip.To4() == nil {
			out = append(out, ip)
		}
	}
	return out
}
