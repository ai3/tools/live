package agent

import (
	"errors"
	"io"
	"log"
	"os"
	"time"

	lbagent "git.autistici.org/ale/lb/agent"
	"github.com/papertrail/go-tail/follower"
)

// We don't really have a good way to get the number of requests from
// NGINX, so we tail its log file and count the lines.
func runNGINXTailer(logPath string, agent *lbagent.Agent, stopCh chan struct{}) {
	// The follower requires the log file to exist, so if we're
	// started before NGINX we might want to wait a bit.
	if err := waitUntilFileExists(logPath, stopCh); err != nil {
		return
	}

	t, err := follower.New(logPath, follower.Config{
		Whence: io.SeekEnd,
		Offset: 0,
		Reopen: true,
	})
	if err != nil {
		log.Printf("tail error: %v", err)
		return
	}

	for {
		select {
		case line := <-t.Lines():
			if line.Bytes() == nil {
				log.Printf("tail aborted: %v", t.Err())
				return
			}
			agent.Inc(1)
		case <-stopCh:
			return
		}
	}
}

func waitUntilFileExists(path string, stopCh chan struct{}) error {
	tick := time.NewTicker(1 * time.Second)
	defer tick.Stop()

	exists := func() bool {
		_, err := os.Stat(path)
		return !os.IsNotExist(err)
	}

	if exists() {
		return nil
	}
	for {
		select {
		case <-tick.C:
			if exists() {
				return nil
			}
		case <-stopCh:
			return errors.New("stopped")
		}
	}
}
