package agent

import (
	"context"

	livepb "git.autistici.org/ai3/tools/live/proto"
	lbpb "git.autistici.org/ale/lb/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// Auxiliary type to set per-request RPC credentials with a shared
// authentication token.
type grpcAuth struct {
	meta map[string]string
}

func newGRPCAuth(authToken string) *grpcAuth {
	return &grpcAuth{
		meta: map[string]string{"authorization": authToken},
	}
}

func (g *grpcAuth) GetRequestMetadata(_ context.Context, _ ...string) (map[string]string, error) {
	return g.meta, nil
}

func (g *grpcAuth) RequireTransportSecurity() bool {
	return true
}

// Implements lbagent.Dialer but using the livepb RPC services.
type grpcDialer struct {
	conn *grpc.ClientConn
	info *livepb.NodeInfo
}

func newGRPCDialer(addr, authToken, caPath string, info *livepb.NodeInfo) (*grpcDialer, error) {
	tlsCreds, err := credentials.NewClientTLSFromFile(caPath, "")
	if err != nil {
		return nil, err
	}
	conn, err := grpc.Dial(
		addr,
		grpc.WithTransportCredentials(tlsCreds),
		grpc.WithPerRPCCredentials(newGRPCAuth(authToken)),
	)
	if err != nil {
		return nil, err
	}
	return &grpcDialer{
		conn: conn,
		info: info,
	}, nil
}

func (d *grpcDialer) Close() {
	d.conn.Close()
}

func (d *grpcDialer) Update(ctx context.Context, status *lbpb.NodeStatus) error {
	client := livepb.NewLiveClient(d.conn)

	req := livepb.UpdateRequest{
		Status: status,
		Info:   d.info,
	}
	_, err := client.Update(ctx, &req)
	return err
}
