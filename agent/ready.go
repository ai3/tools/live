package agent

import (
	"os"
	"time"

	lbagent "git.autistici.org/ale/lb/agent"
)

// Keep watching the guard file to advertise readiness.
func checkReady(guardFile string, agent *lbagent.Agent, stopCh chan struct{}) {
	tick := time.NewTicker(1 * time.Second)
	defer tick.Stop()

	for {
		select {
		case <-tick.C:
			if _, err := os.Stat(guardFile); err != nil {
				agent.SetReady(false)
			} else {
				agent.SetReady(true)
			}
		case <-stopCh:
			return
		}
	}
}
