package agent

import (
	"net"
	"sync"

	livepb "git.autistici.org/ai3/tools/live/proto"
	lbagent "git.autistici.org/ale/lb/agent"
	"git.autistici.org/ale/lb/contrib/bwmon"
	lbpb "git.autistici.org/ale/lb/proto"
)

type Agent struct {
	agent  *lbagent.Agent
	stopCh chan struct{}
	wg     sync.WaitGroup
}

func New(name, addr, authToken, caPath, logPath, readyFile, device string) (*Agent, error) {
	// Create the static NodeInfo for this host, with the list of
	// our public IP addresses.
	addrs, err := nonLocalAddrs(device)
	if err != nil {
		return nil, err
	}
	info := &livepb.NodeInfo{
		NodeName: name,
		IpAddrs:  addrs,
	}

	// Instantiate the lb Agent, talking to the director service
	// via our custom GRPC API.
	dialer, err := newGRPCDialer(addr, authToken, caPath, info)
	if err != nil {
		return nil, err
	}
	a := lbagent.New(name, nil, dialer)
	agent := &Agent{
		agent:  a,
		stopCh: make(chan struct{}),
	}

	// Report used bandwidth on the public device.
	a.RegisterUtilization("bandwidth", lbpb.Utilization_COUNTER, bwmon.GetBandwidthUtilization(device, bwmon.TX))

	// Report request counters by watching the NGINX access log
	// file.
	agent.wg.Add(1)
	go func() {
		runNGINXTailer(logPath, a, agent.stopCh)
		agent.wg.Done()
	}()

	// Report host readiness by watching the guard file.
	agent.wg.Add(1)
	go func() {
		checkReady(readyFile, a, agent.stopCh)
		agent.wg.Done()
	}()

	a.Start()

	return agent, nil
}

func (a *Agent) Close() {
	close(a.stopCh)
	a.agent.Stop()
	a.wg.Wait()
}

// Returns the list of all non-loopback addresses (IPv4 and IPv6) for
// the specified interface.
func nonLocalAddrs(device string) ([]string, error) {
	intf, err := net.InterfaceByName(device)
	if err != nil {
		return nil, err
	}

	addrs, err := intf.Addrs()
	if err != nil {
		return nil, err
	}

	var ips []string
	for _, addr := range addrs {
		ip, _, err := net.ParseCIDR(addr.String())
		if err != nil || ip.IsLoopback() || ip.IsLinkLocalUnicast() {
			continue
		}
		ips = append(ips, ip.String())
	}
	return ips, nil
}
