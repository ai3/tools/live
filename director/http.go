package director

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	livepb "git.autistici.org/ai3/tools/live/proto"
)

var shutdownTimeout = 5 * time.Second

type nodePicker interface {
	PickNode(context.Context) (*livepb.NodeInfo, bool, bool, error)
}

// The HTTP server picks a node on every incoming request, and send a
// redirect to it. Some 'special' URLs are served directly.
type httpServer struct {
	director nodePicker

	origin string
}

func newHTTPServer(director nodePicker, origin string) *httpServer {
	return &httpServer{
		director: director,
		origin:   origin,
	}
}

func (s *httpServer) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// Static content is handled right away.
	switch req.URL.Path {
	case "/favicon.ico":
		w.Header().Set("Content-Type", "image/x-icon")
		w.Header().Set("Content-Length", strconv.Itoa(len(faviconICO)))
		w.Write(faviconICO)
		return
	case "/robots.txt":
		w.Write(robotsTXT)
		return
	}

	target, _, overload, err := s.director.PickNode(req.Context())
	if overload {
		http.Error(w, "Service overloaded", http.StatusServiceUnavailable)
		return
	}
	if err != nil {
		log.Printf("http error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	u := *req.URL
	u.Host = fmt.Sprintf("%s.%s", target.NodeName, s.origin)

	http.Redirect(w, req, u.String(), http.StatusFound)
}

func runHTTPServer(srv *httpServer) func(context.Context, string, string) error {
	return func(ctx context.Context, addr, proto string) error {
		httpsrv := &http.Server{
			Addr:              addr,
			Handler:           srv,
			ReadTimeout:       10 * time.Second,
			ReadHeaderTimeout: 3 * time.Second,
			WriteTimeout:      10 * time.Second,
			IdleTimeout:       30 * time.Second,
		}

		go func() {
			<-ctx.Done()

			// Create an standalone context with a short timeout.
			sctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
			httpsrv.Shutdown(sctx) // nolint
			cancel()
		}()

		err := httpsrv.ListenAndServe()
		if err == http.ErrServerClosed {
			err = nil
		}
		return err
	}
}
