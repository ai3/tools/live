package director

import (
	"context"
	"net"
	"strings"
	"time"

	livepb "git.autistici.org/ai3/tools/live/proto"
	"github.com/miekg/dns"
)

const (
	zoneTTL   = 21600
	nsTTL     = 3600
	recordTTL = 300
)

type nodeGetter interface {
	GetNode(string) *livepb.NodeInfo
}

type dnsServer struct {
	director nodeGetter

	soa         *dns.SOA
	ns          []dns.RR
	origin      string
	originParts int
}

func newDNSServer(director nodeGetter, origin string, nameservers []string) *dnsServer {
	// Create a SOA record for the zone. Some entries will be bogus.
	soa := &dns.SOA{
		Hdr: dns.RR_Header{
			Name:   origin,
			Rrtype: dns.TypeSOA,
			Class:  dns.ClassINET,
			Ttl:    zoneTTL,
		},
		Ns:      ensureFinalDot(nameservers[0]),
		Mbox:    "hostmaster." + origin,
		Serial:  uint32(time.Now().Unix()),
		Refresh: 43200,
		Retry:   3600,
		Expire:  uint32(zoneTTL),
		Minttl:  uint32(zoneTTL),
	}

	// Create the NS records.
	var ns []dns.RR
	for _, nameserver := range nameservers {
		ns = append(ns, &dns.NS{
			Hdr: dns.RR_Header{
				Name:   origin,
				Rrtype: dns.TypeNS,
				Class:  dns.ClassINET,
				Ttl:    nsTTL,
			},
			Ns: ensureFinalDot(nameserver),
		})
	}

	return &dnsServer{
		director:    director,
		soa:         soa,
		ns:          ns,
		origin:      origin,
		originParts: dns.CountLabel(origin),
	}
}

func (d *dnsServer) ServeDNS(w dns.ResponseWriter, req *dns.Msg) {
	m := new(dns.Msg)
	ednsFromRequest(req, m)

	// Only consider the first question. Normalize the request by
	// stripping the origin from the name.
	var name string
	var nameParts []string
	q := req.Question[0]
	if !dns.IsSubDomain(d.origin, q.Name) {
		goto nxDomain
	}
	nameParts = dns.SplitDomainName(q.Name)
	name = strings.ToLower(strings.Join(
		nameParts[:len(nameParts)-d.originParts], "."))

	switch {
	case name == "" && q.Qtype == dns.TypeSOA:
		m.Answer = append(m.Answer, d.soa)

	case name == "" && q.Qtype == dns.TypeNS:
		m.Answer = append(m.Answer, d.ns...)

	case name != "" && (q.Qtype == dns.TypeA || q.Qtype == dns.TypeAAAA):
		// Lookup the name in our node list.
		node := d.director.GetNode(name)
		if node == nil {
			goto nxDomain
		}

		// If no IP matches the protocol, return NXDOMAIN.
		var ips []net.IP
		switch q.Qtype {
		case dns.TypeA:
			ips = node.GetIPv4Addrs()
		case dns.TypeAAAA:
			ips = node.GetIPv6Addrs()
		}
		if len(ips) == 0 {
			goto nxDomain
		}

		for _, ip := range ips {
			var rec dns.RR
			if q.Qtype == dns.TypeAAAA {
				rec = &dns.AAAA{
					Hdr: dns.RR_Header{
						Name:   q.Name,
						Rrtype: dns.TypeAAAA,
						Class:  dns.ClassINET,
						Ttl:    recordTTL,
					},
					AAAA: ip,
				}
			} else {
				rec = &dns.A{
					Hdr: dns.RR_Header{
						Name:   q.Name,
						Rrtype: dns.TypeA,
						Class:  dns.ClassINET,
						Ttl:    recordTTL,
					},
					A: ip,
				}
			}
			m.Answer = append(m.Answer, rec)
		}

	case q.Qtype == dns.TypeANY:
		goto servFail

	default:
		goto nxDomain
	}

	m.SetReply(req)
	m.MsgHdr.Authoritative = true
	w.WriteMsg(m) //nolint
	return

servFail:
	m.SetRcode(req, dns.RcodeServerFailure)
	w.WriteMsg(m) //nolint
	return

nxDomain:
	m.SetRcode(req, dns.RcodeNameError)
	w.WriteMsg(m) //nolint
}

// Create skeleton edns opt RR from the query and add it to the
// message m.
func ednsFromRequest(req, m *dns.Msg) {
	for _, r := range req.Extra {
		if r.Header().Rrtype == dns.TypeOPT {
			m.SetEdns0(4096, r.(*dns.OPT).Do())
			return
		}
	}
}

func ensureFinalDot(s string) string {
	if !strings.HasSuffix(s, ".") {
		return s + "."
	}
	return s
}

func runDNSServer(srv *dnsServer) func(context.Context, string, string) error {
	return func(ctx context.Context, addr, proto string) error {
		dnssrv := &dns.Server{
			Addr:    addr,
			Net:     proto,
			Handler: srv,
		}

		go func() {
			<-ctx.Done()

			// Create an standalone context with a short timeout.
			sctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
			dnssrv.ShutdownContext(sctx) // nolint: errcheck
			cancel()
		}()

		return dnssrv.ListenAndServe()
	}
}
