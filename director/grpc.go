package director

import (
	"context"
	"net"

	livepb "git.autistici.org/ai3/tools/live/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// Handle shared-token authentication from clients.
func authServerInterceptor(token string) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Errorf(codes.InvalidArgument, "Failed to retrieve request metadata")
		}

		authHeader, ok := md["authorization"]
		if !ok {
			return nil, status.Errorf(codes.Unauthenticated, "Missing authorization token")
		}
		if authHeader[0] != token {
			return nil, status.Errorf(codes.Unauthenticated, "Authentication failed")
		}

		return handler(ctx, req)
	}
}

func runGRPCServer(d *Director, authToken, certFile, keyFile string) func(context.Context, string, string) error {
	return func(ctx context.Context, addr, proto string) error {
		creds, err := credentials.NewServerTLSFromFile(certFile, keyFile)
		if err != nil {
			return err
		}

		grpcServer := grpc.NewServer(
			grpc.Creds(creds),
			grpc.UnaryInterceptor(authServerInterceptor(authToken)),
		)
		livepb.RegisterLiveServer(grpcServer, d)

		l, err := net.Listen(proto, addr)
		if err != nil {
			return err
		}

		go func() {
			<-ctx.Done()
			grpcServer.GracefulStop()
		}()

		return grpcServer.Serve(l)
	}
}
