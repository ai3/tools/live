package director

import (
	"context"
	"log"
	"sync"

	livepb "git.autistici.org/ai3/tools/live/proto"
	"git.autistici.org/ale/lb"
	lbpb "git.autistici.org/ale/lb/proto"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/empty"
)

type Director struct {
	*lb.Loadbalancer

	mx    sync.Mutex
	nodes map[string]*livepb.NodeInfo
}

func (d *Director) Update(ctx context.Context, req *livepb.UpdateRequest) (*empty.Empty, error) {
	d.mx.Lock()
	if _, ok := d.nodes[req.Info.NodeName]; !ok {
		log.Printf("discovered new node %s", req.Info.NodeName)
	}
	d.nodes[req.Info.NodeName] = proto.Clone(req.Info).(*livepb.NodeInfo)
	d.mx.Unlock()

	return d.Loadbalancer.Update(ctx, req.Status)
}

func (d *Director) GetNode(name string) *livepb.NodeInfo {
	d.mx.Lock()
	defer d.mx.Unlock()
	return d.nodes[name]
}

func (d *Director) PickNode(ctx context.Context) (*livepb.NodeInfo, bool, bool, error) {
	resp, err := d.Loadbalancer.Select(ctx, &lbpb.SelectRequest{})
	if err != nil {
		return nil, false, false, err
	}

	d.mx.Lock()
	defer d.mx.Unlock()
	return d.nodes[resp.NodeName], resp.Overflow, resp.Overload, nil
}
